<?php

/**
 * 
 */
class View
{
	private static $defaultLayout = 'layout.php';
	private static 	$_viewPath = 'app/views/',
	 				$_localizationPath = 'localization/',
					$_controller = '', 
					$_view = '',
					$_data = [],
					$_localization = [];

	public static function loadView($view, $controller, $data = [], $layout = null)
	{
		self::$_controller = $controller;
		self::$_view = $view;
		self::$_data = $data;
		
		//	Load layout		
		if(isset($layout))				
			$l = self::getFileContent(self::$_viewPath . "shared/$layout.php");		
		else
			$l = self::getFileContent(self::$_viewPath . "shared/".self::$defaultLayout);
		
		
		//	Load localization
		self::$_localization = Localization::getLocalization($controller);		
		
		echo self::processView($l);		
	}


	private static function processView($view)
	{
		//	Apply localization and model
		$viewFunc = '';
		$viewFuncData = '';
		$htmlString = '';
		$locReplace = '';
		$viewFuncEnable = false;
		$locReplaceEnable = false;
		$dataReplaceEnable = false; 	
		
		$foreachVariable = '';
		$foreachEnable = false;	
					
		$view = str_split($view);
		foreach ($view as $key => $value) 
		{
			//	View Functions
			if($value == '@')
			{
				if($viewFuncEnable)
					throw new Exception("Wrong view function deficition", 1);

				$viewFuncEnable = true;
				continue;					
			}

			//	View function end
			if($value == ')' && $viewFuncEnable) 
			{
				$viewFunc = strtolower($viewFunc);

				if($viewFunc == 'renderbody(')
				{
					//	Load view
					$v = self::getFileContent(self::$_viewPath . self::$_controller.'/'.self::$_view.".php");
					$htmlString .= self::processView($v);
				}

				if(strpos($viewFunc, 'section') !== false)
				{	
					//	get parameters
					//	TODO optimize!!!
					$params = self::getFuncParams($viewFunc);

					if(strlen($params) == 0)
						throw new Exception("Wrong parameters in 'Section' function", 1);

					//	Load content
					$v = self::getFileContent(self::$_viewPath . $params);
					$htmlString .= self::processView($v);					
				}

				if(strpos($viewFunc, 'anticsfr') !== false)
				{
					include_once 'app/core/security/csfr.php';
					$htmlString .= AntiCSFR::generateFormToken();
				}

				if(strpos($viewFunc, 'validationmsg') !== false)
				{
					$htmlString .= Session::getValidationMsg();
				}

				if(strpos($viewFunc, 'model') !== false)
				{

					$params = self::getFuncParams($viewFunc);

					if(strpos($params, '/') !== false)
					{						
						$split = explode('/', $params);
						$tt = $split[1];
						if(isset(self::$_data[$split[0]]))
							if(is_array(self::$_data[$split[0]]))
								$htmlString .= isset(self::$_data[$split[0]][$tt])? self::$_data[$split[0]][$tt] : '';	
							else
								$htmlString .= isset(self::$_data[$split[0]]->$tt)? self::$_data[$split[0]]->$tt : '';							
					}
					else
						$htmlString .= isset(self::$_data[$params])? self::$_data[$params] : '';
				}

				//	BIG TODO
				if(strpos($viewFunc, 'foreach') !== false && !$foreachEnable)
				{
					$params = self::getFuncParams($viewFunc);
					
					$foreachVariable = $params;					
					$foreachEnable = true;
				}
				//	BIIIG TODO
				if(strpos($viewFunc, 'endforeach') !== false && $foreachEnable)
				{	
					
					if(is_array(reset(self::$_data)))
						$data = self::$_data[$foreachVariable];
					else
						$data = self::$_data;
					
					foreach ($data as $value) 
					{
						$resolvedData = $viewFuncData;
						foreach (str_split($viewFuncData) as $idx => $char) {
							if($char == '$')
							{
								$tmpStr = trim(substr($viewFuncData, $idx));
								$variable = substr($tmpStr, 1, 									
										min(
											self::strPos($tmpStr, '<'), 
											self::strPos($tmpStr, '"'), 
											self::strPos($tmpStr, '>'), 
											self::strPos($tmpStr, ' '),
											self::strPos($tmpStr, '('),
											self::strPos($tmpStr, ')'),
											self::strPos($tmpStr, '/'),
											self::strPos($tmpStr, '\\'),
											self::strPos($tmpStr, ':'),
											self::strPos($tmpStr, ';'),
											self::strPos($tmpStr, '\'')
									)-1
								);	

								//var_dump($variable);

								$resolvedData = str_replace('$'.$variable, $value->$variable, $resolvedData);								
							}
						}
						//var_dump($resolvedData);
						$htmlString .= self::processView($resolvedData);
						// $htmlString .= $resolvedData;
					}
					
					$viewFuncData = '';
					$foreachEnable = false;
				}

				//	Reset
				$viewFunc = '';
				$viewFuncEnable = false;
				continue;
			}

			//	View function wrong definition
			if($viewFuncEnable && $value == ' ')
			{
				throw new Exception("Wrong view function deficition", 1);
			}

			//	Localization start
			if($value == '{' && !$foreachEnable)
			{
				if($dataReplaceEnable)
					throw new Exception("Wrong view localization markings", 1);
				
				if($locReplaceEnable)
					continue;

				if(!$locReplaceEnable && $view[$key+1] == '{')
				{
					$locReplaceEnable = true;					
					continue;
				}
			};

			//	Model/Localization end
			if(($value =='}' && $view[$key+1] == '}' && $locReplaceEnable))
			{
				$locReplace = strtolower(trim(str_replace('}', '', $locReplace)));				
				$htmlString .= isset(self::$_localization[$locReplace])? self::$_localization[$locReplace]: '';				
				
				$locReplace = '';
				continue;
			};

			if($value == '}' && $view[$key-1] == '}' && $locReplaceEnable)
			{
				$locReplaceEnable = false;
				continue;
			}

			if($locReplaceEnable || $dataReplaceEnable)
				$locReplace .= $value;
			else if($viewFuncEnable)
				$viewFunc .= $value;	
			else if($foreachEnable)		
				$viewFuncData .= $value;
			else
				$htmlString .= $value;

		};

		return $htmlString;
	}

	private static function getFileContent($path)
	{
		$ctx = stream_context_create(array( 
		    'http' => array( 
		        'timeout' => 1 
		        ) 
		    ) 
		); 

		try {

			return $l = file_get_contents($path, 0, $ctx);	

		} catch (Exception $e) {
			throw new Exception("View not found!!!", 1);
			
		}
		
	}

	public static function setLayout($layout)
	{
		self::$defaultLayout = $layout;
	}


	private static function getFuncParams($viewFunc)
	{
		$params = substr($viewFunc, strpos($viewFunc, '(')+1);				
		return str_replace("'", "", $params);
	}

	private static function strPos($haystack, $needle)
	{
		return strpos($haystack, $needle) !== false? strpos($haystack, $needle) : 99999999;
	}
}
