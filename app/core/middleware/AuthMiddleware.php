<?php
include_once 'IMiddleware.php';

/**
 * 
 */
class AuthMiddleware implements IMiddleware
{
	
	/**
	*	Middleware handle function
	*/
	public static function handle($options = [])
	{
		if(!Auth::isAuthorized() || !Auth::checkRole($options['role']))
			Redirect::route('login');

	}
}