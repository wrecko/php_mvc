<?php

/**
 *  Core app class
 */
class App
{
	protected 	$controller = 'Home',
	 			$method = 'index',
	 			$params = [];

	public static $locale = 'en';

	private $routeMiddleware = [
		'auth' => AuthMiddleware::class
	];

	function __construct()
	{

		$request = new Request;		
		$url = $this->parseUrl($request->url);

		$savedRoute = Route::findRoute($request->url);

		//	Brute force protection check
		if(BTProtection::isBlocked()){			
			if($savedRoute == null || ($savedRoute != null && $savedRoute->controller != 'ErrorController'))			
				Redirect::route('error');
		}

		if($savedRoute != null)
		{
			//	Middleware execution
			if(!empty($savedRoute->middleware))
			{
				if(!array_key_exists($savedRoute->middleware, $this->routeMiddleware))
					throw new Exception("Middleware: '$savedRoute->middleware', is not registered!!", 1);
					
				$this->executeMiddleware($savedRoute->middleware, $savedRoute->middlewareOptions);
			}

			// Lang setting
			if(!empty($savedRoute->locale))
				self::setLocale($savedRoute->locale);

			// Route execution
			if($savedRoute->callback != null)
				$savedRoute->executeCallback();
			else
			{
				$params = [$savedRoute->controller, $savedRoute->method];				
				if(count($url) > 2) $params = array_merge($params, array_slice($url, 2));				
				$this->callController($params, $request);
			}
		}
		else
		{
			$this->callController($url, $request);
		}

	}

	function parseUrl($url)
	{
		if($url)
		{
			return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
			//return explode('/', $_GET['url']);
		}
	}

	function callController($url, $request)
	{
		if(file_exists('app/controllers/' . $url[0] . '.php' ))
		{
			$this->controller = $url[0];
			unset($url[0]);
			$url = array_values($url);
		}

		require_once 'app/controllers/' . $this->controller . '.php';		

		$this->controller = new $this->controller;		

		if(isset($url[0]))
		{
			
			if(method_exists($this->controller, $url[0]))
			{
				$this->method = $url[0];
				unset($url[0]);
			}
			else
			{
				return Redirect::route('404');
			}
		}

		if($request->method == Request::POST)
		{
			//	TODO:: set model arg parameter

			array_push($this->params, $request);
		}
		else
			$this->params = $url ? array_values($url) : [];

		call_user_func_array([$this->controller, $this->method], $this->params);
	}

	private function executeMiddleware($middleware, $options)
	{

		require_once "middleware/". $this->routeMiddleware[$middleware].".php";

		$this->routeMiddleware[$middleware]::handle($options);
	}


	public static function getLocale()
	{
		return self::$locale;
	}
	public static function getDefaultLocale()
	{
		return Config::get('default_lang');
	}
	public static function setLocale($lang)
	{
		self::$locale = $lang;
	}
}		


