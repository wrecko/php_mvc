<?php 

/**
 * 
 */
class RouteModel
{
	
	public $route;
	public $callback;
	public $controller;
	public $method;
	public $actionMethod;

	public $middleware;
	public $middlewareOptions;

	public $locale;
	
	

	function __construct($route, $actionMethod, $callback = null, $controller = '', $method = '', $locale = 'en')
	{
		$this->route = $route;
		$this->callback = $callback;
		$this->controller = $controller;
		$this->method = $method;
		$this->actionMethod = $actionMethod;		
		$this->locale = $locale;
	}

	public function middleware($name, $options = [])
	{
		// if(!is_array($name))
		// 	$name = [$name];
		$this->middleware = $name;
		$this->middlewareOptions= $options;

		return $this;
	}

	public function executeCallback()
	{
		$callback = $this->callback;
		$callback();
	}
}

