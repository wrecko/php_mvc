<?php 

/**
 * Routing class
 */
class Route
{
	
	private static $routes = [];
	private static $middleware;
	private static $middlewareOptions;
	private static $locale;

	
	public static function get($uri, $callback)
	{		
		$route = null;
		$actionMethod = 'GET';
		if(is_callable($callback))
			$route = (new RouteModel($uri, $actionMethod, $callback))->middleware(self::$middleware, self::$middlewareOptions);
		else if (preg_match("/[a-zA-Z]@[a-z0-9]/", $callback))
		{
			$controllerRoute = explode('@', $callback);
			if(count($controllerRoute) > 2)
				throw new Exception("Invalid controller route", 1);
			$route = (new RouteModel($uri, $actionMethod, null, $controllerRoute[0], $controllerRoute[1], self::$locale))->middleware(self::$middleware, self::$middlewareOptions);				
		}

		self::$routes[] = $route;
		return $route;
	}

	public static function post()
	{
		
	}

	public static function put()
	{

	}

	public static function delete()
	{

	}

	public static function middleware($name, $options = [])
	{
		if(!is_array($options))
			throw new Exception("Wrong setted middleware options!!!", 1);
			
		self::$middleware = $name;
		self::$middlewareOptions = $options;
		return new static();
		
	}


	public static function getAll()
	{
		return self::$routes;
	}

	public static function findRoute($uri)
	{
		foreach (self::$routes as $key => $value) {			
			if($value->route == $uri){
				return $value;
			}
		}
		return null;
	}

	public static function locale($lang, $callback)
	{
		if(!is_callable($callback))
			throw new Exception("Wrong route config!!!", 1);
		
		//App::setlocale($lang);
		self::$locale = $lang;
		$callback();
		return new static();
	}

	public function group($callback)
	{
		if(!is_callable($callback))
			throw new Exception("Wrong route config!!!", 1);

		$callback();
		self::$middleware = '';
		self::$middlewareOptions = [];
			
	}


}
