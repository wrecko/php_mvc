<?php

class Redirect
{
	public static function toAction($action, $model = null)
	{		
		Session::saveModel($model);
		$controller = get_called_class();
		header("Location: /$controller/$action");
	}

	public static function toControllerAction($action, $controller, $model = null)
	{
		Session::saveModel($model);
		header("Location: /$controller/$action");
	}

	public static function route($route = '', $model = null)
	{
		Session::saveModel($model);
		header("Location: /$route");
	}
}
