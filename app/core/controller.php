<?php
require_once 'views/view.php';
/**
 *  Controller base class
 */
abstract class Controller
{
	protected function register_models($names = []){
		foreach ($names as $key => $model) {
			//$this->model($value);
			require_once 'app/models/' . $model . '.php';
		}
	}

	protected function model($model)
	{
		require_once 'app/models/' . $model . '.php';
		return new $model();
	}

	protected function view($view = "index", $data = [])
	{
		$controller = get_called_class();
		View::loadView($view, $controller, $data);	
	}

	protected function authorize($role)
	{
		include_once 'middleware/AuthMiddleware.php';
		AuthMiddleware::handle($role);
	}

	protected function toAction($action, $model = null)
	{
		Redirect::toControllerAction($action, get_called_class(), $model);
	}

}