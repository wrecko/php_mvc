<?php

/**
 * 
 */
class Request
{
		
	const 	POST = 'post',
			GET = 'get';

	public 	$method,
	 		$postData,
			$files,
			$url,
			$csfrValidated = false,
			$ipAddress;

	function __construct()
	{
		include_once 'app/core/security/csfr.php';
		
		$server = $_SERVER;
		$req_post = $_POST;
		$req_get = $_GET;
		$req_files = $_FILES;

		$this->method = ($req_post == [])? Request::GET : Request::POST;			
		$this->url = isset($req_get['url'])? $req_get['url'] : null;	
		
		if($this->method == Request::POST && isset($req_post[antiCSFR::TOKEN_NAME]) && antiCSFR::validateToken($req_post[antiCSFR::TOKEN_NAME]))
		{
			$this->csfrValidated = true;
			$this->postData = $req_post;
		}

		$this->ipAddress = self::getUserIP();		
	}

	public function all() {return $this->postData;}

	public function validate() { return $this->csfrValidated; }



	public static function getUserIP()
	{
	    // Get real visitor IP behind CloudFlare network
	    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
	              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	    }
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}
}