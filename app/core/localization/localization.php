<?php

/**
 * 
 */
class Localization 
{
	
	private static $_localizationPath = 'localization/';


	public static function getErrorString($id, $locale = 'en')
	{
		$path = self::$_localizationPath.$locale.'/error.php';
		return self::loadLocalizationFile($path)[$id];
	}

	public static function getLocalization($file)
	{
		$locale = App::getLocale();	
		
		if(!file_exists(self::$_localizationPath."$locale/$file.php"))
			$locale = App::getDefaultLocale();
		
		return self::loadLocalizationFile(self::$_localizationPath."$locale/$file.php");
	}

	public static function getValidationError($error = 'general_validation_error')
	{
		return self::getErrorString($error);
	}


	private static function loadLocalizationFile($path)
	{
		try 
		{
			if(file_exists($path))
				return include_once $path;	

		} catch (Exception $e) {
			throw new Exception("Couldn't find localization file", 1);
			
		}
	}
}