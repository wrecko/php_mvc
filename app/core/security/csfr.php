<?php 


/**
 * 
 */
class antiCSFR
{

	public const TOKEN_NAME = 'anti-csfr-token';

	public static function generateFormToken()
	{
		$token = bin2hex(random_bytes(64));

		$_SESSION[self::TOKEN_NAME] = $token;	
		
		return "<input type=\"hidden\" name=\"".self::TOKEN_NAME."\" value=\"$token\">";
	}

	public static function validateToken($postToken)
	{
		if(!empty($_SESSION[self::TOKEN_NAME]) && $_SESSION[self::TOKEN_NAME] == $postToken)
		{
			 unset($_SESSION[self::TOKEN_NAME]);
			 return true;
		}
		return false;
	}
}