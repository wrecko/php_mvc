<?php 

/**
 * 
 */
class Auth
{
	private static $userTable = 'user';
	private static $sessionTable = 'user';
	private const PASSWORDCOST = 12;
	
	public static function isAuthorized()
	{
		return isset($_SESSION['user_id']); /*&& isset($_COOKIE['user_id']) && isset($_COOKIE['token']) && isset($_COOKIE['serial'])*/;
	}

	public static function checkRole($role)
	{
		return isset($_SESSION['role']) && $_SESSION['role'] == $role;
	}

	public static function authorize($username, $password)
	{
		if(self::isAuthorized())
			return false;
		
		$db = Database::getInstance();
		$dbResult = $db->select(self::$userTable, ['username' => "$username"]);
		//$sessionExists = $db->select(self::$sessionTable, ['session_id' => session_id()]);
		
		if($dbResult->success && password_verify($password, $dbResult->data[0]['password']))
		{			
			$_SESSION['user_id'] = $dbResult->data[0]['id'];
			$_SESSION['role'] = $dbResult->data[0]['role'];

			//self::createCookies($dbResult->data[0]['id'], $dbResult->data[0]['role'], '', '');
			
			return true;
		}
		return false;
	}

	public static function logout()
	{
		$_SESSION = array();
		self::removeCookies();

		Redirect::route('login');
	}

	public static function register($user)
	{
		$db = Database::getInstance();
		
		// Check if username exists
		if($db->select(self::$userTable, ['username' => "'$user->username'"])->success)
			throw new Exception("Username '$user->username'  exist in DB!", 1);			

		$options = [
			'cost' => self::PASSWORDCOST,
		];

		if(!is_array($user))
		{
			$user->password = password_hash($user->password, PASSWORD_BCRYPT, $options);			
			return $user->save();
		}
		
		$user['password'] = password_hash($user['password'], PASSWORD_BCRYPT, $options);		
		return $db->create(self::$userTable, $user);
	}

	public static function user()
	{
		$db = Database::getInstance();
		return self::isAuthorized()? $db->select(self::$userTable, ['id' => $_SESSION['_user_id']])->data[0] : null;
	}	

	public static function id()
	{
		return isset($_SESSION['user_id'])? $_SESSION['user_id'] : -1;
	}

	public static function role()
	{
		return isset($_SESSION['role'])? $_SESSION['role'] : null;
	}

	public static function isAdmin()
	{
		return Auth::isAuthorized()? Auth::role() == ENUMS::ADMIN : false;
	}

	private static function createCookies($userId, $userRole, $token, $serial)
	{
		setcookie('user_id', $userId, time() + (86400) * 30, "/");
		setcookie('userRole', $userRole, time() + (86400) * 30, "/");
		// setcookie('token', $token, time() + (86400) * 30, "/");
		// setcookie('serial', $serial, time() + (86400) * 30, "/");
	}

	private static function removeCookies()
	{	
		setcookie('user_id', "", time() - 1, "/");
		setcookie('userRole', "", time() - 1, "/");
		setcookie('token', "", time() - 1, "/");
		setcookie('serial', "", time() - 1, "/");
	}


}
