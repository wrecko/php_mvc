<?php 

/**
 * 
 */
class BTProtection
{

	const 	SESSION_BT_NAME = 'bt',
			DB_TABLE_NAME = 'btsessionblocked',
			SESSION_CONF_TIME = 'bruteForceTimeLimit',
			SESSION_CONF_TRY = 'bruteForceTryLimit';

	public static function isBlocked()
	{	
		$db = Database::getInstance();
		$dbFound = false;

		//	Get session data
		$bt = Session::get(self::SESSION_BT_NAME);

		//	If bt data not found in session, look in DB
		if(!isset($bt))
		{			
			$dbRes = $db->select(self::DB_TABLE_NAME, ['ip' => Request::getUserIp()]);
			if(!$dbRes->success)
				return false;
			$bt = $dbRes->data[0];
			$bt['blocked'] = boolval($bt['blocked']);
			$dbFound = true;			
		}	

		$timeBlock = Config::get(self::SESSION_CONF_TIME)*60;		
		$tryLimit = Config::get(self::SESSION_CONF_TRY);

		if(microtime(true)-$bt['lastTry'] > $timeBlock)
		{
			Session::unset(self::SESSION_BT_NAME);
			$db->delete(self::DB_TABLE_NAME, ['ip' => [$bt['ip']]]);

			return false;
		}

		if($bt['blocked'] || (!$bt['blocked'] && $bt['tries'] >= $tryLimit))
		{
			$bt['blocked'] = true;
			Session::set(self::SESSION_BT_NAME, $bt);
			
			$db->update(self::DB_TABLE_NAME, $bt, ['ip' => $bt['ip']]);

			return true;
		}
		
	}	

	public static function check($request)
	{
		$db = Database::getInstance();

		$ip = $request->ipAddress;
		$time = microtime(true);		
		$tries = 0;
		$lastSet = Session::get(self::SESSION_BT_NAME);


		if(isset($lastSet))
		{
			$tries = $lastSet['tries'];
		}

		$data = ['ip' => $ip, 'lastTry' => $time, 'tries' => $tries+1, 'blocked' => false ];
		Session::set(self::SESSION_BT_NAME, $data);

		if($db->select(self::DB_TABLE_NAME, ['ip' => $ip])->success)
		{
			$db->update(self::DB_TABLE_NAME, $data, ['ip' => $ip]);
		}
		else
		{
			$db->create(self::DB_TABLE_NAME, $data);	
		}

	}

}