<?php

/**
 * 
 */
class Session 
{
	
	public static function getSavedModel()
	{ 
		if(!isset($_SESSION['model']))
			return null;

		$ret = $_SESSION['model'];
		$_SESSION['model'] = null;

		return $ret;
	}

	public static function saveModel($model)
	{
		if($model == null)
			return;
		$_SESSION['model'] = $model;
	}

	public static function getValidationMsg()
	{
		//	Get Session data
		$data = self::getSavedModel();

		if($data instanceof BaseModel)
			return $data->validationMsg;
		else
			return $data;

	}

	public static function get($name)
	{
		return isset($_SESSION[$name])? $_SESSION[$name] : null;
	}

	public static function set($name, $val)
	{
		$_SESSION[$name]=$val;
	}
	public static function unset($name)
	{
		$_SESSION[$name] = null;
	}
}