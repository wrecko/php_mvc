<?php

/**
 * 
 */
class Config
{
	
	private static $_loadedConfig = [];

	public static function get($name)
    {
    	if(empty(self::$_loadedConfig))
    	{
    		
    		self::$_loadedConfig = include_once 'app/config/config.php';
    	}
    	
    	return self::$_loadedConfig[$name];
    }

}