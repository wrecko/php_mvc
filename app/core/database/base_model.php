<?php
/**
 * 
 */
abstract class BaseModel
{
	
	public $id = '';
	public $validationMsg = '-';
	protected $table = '';
	protected $hasId = true;
	
	//private $savedModel = null;
	protected static $ignoreProps = ['table', 'timestamp', 'id', 'hasId', 'validationMsg'];
	protected static $dateNowProps = ['updated_at', 'created_at'];
	

	function __construct()
	{
		
	}


	/**
	*
	*/
	public static function all()
	{
		$db = Database::getInstance();
		
		$model = new static();
		$dbResult = $db->Select($model->table);
		//var_dump($dbResult);
		$properties = get_object_vars($model);

		$results = [];
		foreach ($dbResult->data as $value) {

			$model = new static();
			$rec = $value;		
						
			foreach ($properties as $key => $prop) {
		 		if($prop != null || ($key == 'id' && !$model->hasId))
			 		continue;
			 	$val = $rec[$key];
			 	$model->$key = $val;  
			};

			array_push($results, $model);
		};		
		//var_dump($results);
		
		return $results;
	}

	public static function find($id)
	{
		$db = Database::getInstance();
		
		$model = new static();
		$dbResult = $db->Select($model->table, ['id' => $id]);
		$properties = get_object_vars($model);
		if(!$dbResult->success)
			throw new Exception("Couldn't find record with ID: $id", 1);
			
		$rec = $dbResult->data[0];		
		foreach ($properties as $key => $prop) {
	 		if($prop != null)
		 		continue;
		 	$val = $rec[$key];
		 	$model->$key = $val;  
		};
		//var_dump($model);
		return $model;
	}

	public static function create($model)
	{
		$db = Database::getInstance();
		$obj = new static();
		$modelProps = get_object_vars($obj);

		//Fill created_at
		// if(is_array($model))
		// 	$model['created_at'] = 'now()';
		// else
		// 	$model->created_at = 'now()';

		$data = [];
		foreach ($model as $key => $value) {
			if(in_array($key, self::$ignoreProps) || $value == null)
	 			continue;
	 		if(!array_key_exists($key, $modelProps))
	 			throw new Exception("Key: '$key', could not be found in model ");
	 		
	 		$data[$key] = $value;
		}

		$dbResult = $db->create($obj->table, $data);

		return ($dbResult->success)? $model : null;

	}

	public static function update($model)
	{
		$db = Database::getInstance();		
		$modelProps = get_object_vars(new static());
		
		if(!is_array($model))
			$model = [$model];
		$result = true;
		foreach ($model as $value) 
		{	
			// $update = '';
			// $data = [];
			if(in_array('updated_at', $modelProps))
				$value->updated_at = 'now()';

			$result = $db->update($value->table, $value)->success;
		}
		return $result? $model : null;
	}

	/**
	*	Removes record from DB by ID
	*/
	public static function delete($id)
	{
					
		$model = new static();
		
		if(!is_array($id))
			$id = [$id];

		$db = Database::getInstance();
		$dbResult = $db->delete($model->table, ['id' => $id]);		
		return $dbResult->success;
		
	}	

	public static function where($column, $value)
	{
		$db = Database::getInstance();
		$model = new static();
		$modelProps = get_object_vars($model);
		$dbResult = $db->Select($model->table, [$column => $value]);

		$result = [];
		foreach ($dbResult->data as $rec) 
		{
			$model = new static();	
			foreach ($modelProps as $key => $prop) 
			{
				if(!array_key_exists($key, $rec))
	 				continue;

				$model->$key = $rec[$key];
			}
			array_push($result, $model);
		}


		return $result;
	}

	public static function orderBy($id)
	{

	}

	public static function cursor($id)
	{

	}

	public static function fillModel($postData)
	{
		$model = new static();
		$modelArgs = get_object_vars($model);
		foreach ($modelArgs as $key => $value) {
			if(!array_key_exists($key, $postData))
				continue;
			
			$model->$key = empty($postData[$key])? null : $postData[$key];
		}
		return $model;
	}
	


	/**
	*	Saves/Updates current model
	*/
	public function save()
	{
		$db = Database::getInstance();
		$model = get_object_vars($this);

		foreach ($model as $key => $value) {
			if(in_array($key, self::$ignoreProps) || in_array($key, self::$dateNowProps))
				unset($model[$key]);
		}

		if($this->id != '')
		{
			$model[$dateNowProps[0]] = 'now()';
			$dbResult = $db->update($this->table, $model);
		}
		else
			$dbResult = $db->create($this->table, $model);	
		
		

		if($this->id == '' and $dbResult->success)
			$this->id = $dbResult->lastInsertId;

		return $this;

	}	

	/**
	*	Removes loaded instance from DB
	*/
	public function remove()
	{
		$db = Database::getInstance();
		$dbResult = $db->query("DELETE FROM $this->table WHERE id = $this->id");
		return $dbResult->success;
	}



}