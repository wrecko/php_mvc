<?php

/**
 * 
 */
class DBResult
{
	public $success;
	public $data;
	public $lastInsertId;
	function __construct($success, $data, $lastInsertId = 0)
	{
		$this->success = $success;
		$this->data = $data;
		$this->lastInsertId = $lastInsertId;
	}
}