<?php
require_once 'db_result.php';
// require_once '../../../../config/config.php';

/**
 * 
 */
class Database 
{
	private static $instance = null;
	
	private $conn;

	function __construct()
	{

		$host = Config::get('dbHost');
		$user = Config::get('dbUser');
		$pass = Config::get('dbPassword');
		$name = Config::get('dbTableName');

		$this->conn = new PDO("mysql:host={$host};
	    dbname={$name}", $user,$pass,
	    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
	    // if(Configuration::dbErrorMode)
	    	$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	}


	public static function getInstance()
	{
		if(self::$instance == null)
		{
			self::$instance = new Database;
		}

		return self::$instance;
	}

	public function getConnection()
	{
		return $this->conn;
	}

	/**
	* Selects all data from table 
	*/
	public function select($table, $where = [])
	{
		$whereDB = '';

		if($where != [])
		{
			$whereCount = count($where);
			
			$whereDB = 'WHERE ';
			$count = 0;
			foreach ($where as $key => $value) {
				//$whereDB .= $key . '=' . $value;
				$whereDB .= $key . '=?';
				if($whereCount > 1 && $whereCount <= $count)
					$whereDB .= ' AND ';
				$count++;
			}
		}
		
		return $this->query('SELECT * FROM '. $table . ' '. $whereDB, array_values($where), false);
	}

	public function create($table, $data)
	{
		$columns = '';
		$values = '';
		foreach ($data as $key => $value) 
		{
			if($key == 'id' || $key == 'updated_at') continue;
			$columns .= "$key,";
	 		//$values .= ($key == 'created_at')? "now()," : "'$value',";
		} 		
		
		$in = str_repeat('?,', count($data) - 1) . '?';
 		$query = "INSERT INTO ".$table." (".substr($columns, 0, -1).") VALUES ($in)";
 		
 		return $this->query($query, array_values($data));

		// $in = str_repeat('?,', count($data) - 1) . '?';
 		// 	$query = "INSERT INTO ".$table." ($in) VALUES ($in)";
 		// 	return $this->query($query, array_merge(array_keys($data), array_values($data)));
	}

	public function update($table, $data, $where = [])
	{
		$query = "UPDATE $table SET ";

		$values = [];
		foreach ($data as $key => $value) 
		{
			if($key == 'id')
				continue;
			$query .= "$key = ?,";
			array_push($values, is_bool($value)? +$value : $value );
		};

		$query = substr($query, 0,-1);

		if(empty($where))
		{
			$query .= " WHERE id = ?";
			if(is_array($data))
				$id = $data['id'];
			else
				$id = $data->id;

			array_push($values, $id);	
		}
		else
		{
			$query .= " WHERE ";
			foreach ($where as $key => $value) {
				$query .= "$key = '$value'";
			}
		}	
		return $this->query($query, $values);
	}

	public function delete($table, $data)
	{	
		$result = null;
		foreach ($data as $key => $value) {
			$in = str_repeat('?,', count($value) - 1) . '?';
			$result = $this->query("DELETE FROM $table WHERE $key in ($in)", $value);
		}
		
		return $result;
	}

	public function query($query, $data = [], $orderSelect = false)
	{
		 	
		$isFetch = preg_match("/select/i", $query);
		// echo $query;
		// var_dump($data);	

		$orderBy='';
		if($orderSelect)
		{
			$orderBy = ' ORDER BY id ASC';
		}
		
		try 
		{
			$stmt = $this->conn->prepare($query . $orderBy);
			$stmt->execute($data);
			if (!$stmt) {
			    echo "\nPDO::errorInfo():\n";
			    print_r($pdo->errorInfo());
			}

			$stmt->setFetchMode(PDO::FETCH_ASSOC); 

			return new DBResult(
				$stmt->rowCount() > 0, 
				($isFetch)? $stmt->fetchAll() : null, 
				$this->conn->lastInsertId()
			);
		} 
		catch (PDOException $e) 
		{
			echo "Error: " . $e->getMessage();	
		}
	}
}