<?php

class Home extends Controller
{
	
	function __construct()
	{
		$this->register_models(['user']);

	}

	public function index($name = '', $sur = '')
	{
		//var_dump(Route::getAll());
		$this->view('index', ['name' => $name, 'sur' => $sur]);
	}

	public function rep($name = '', $sur = '')
	{
		

		$this->view('index', ['name' => 'asdasdas', 'sur' => $sur]);
	}

	public function createGet()
	{
		
		$this->view('home/create', ['name' => '']);
	}

	public function createPost(Request $request)
	{
		$time = microtime(true);
		$user = User::fillModel($request->all())->save();

		echo microtime(true)-$time;
		var_dump($user);
	}

	public function logged()
	{

		if(Auth::isAuthorized())
			$this->view('home/logged');
		else
			header('Location: http://localhost/php_mvc_framework/');
	}

	public function register()
	{
		$user = new User;
		$user->username = "Wreckoo";
		$user->password = 'test';
		$user->role = 1;

		Auth::register($user);
		// /User::create($user)

	}
	public function redirectTest()
	{
		Redirect::toAction('Home', 'index');
	}

	public function login()
	{
		if(!Auth::authorize('Wreckoo', 'test'))
			echo 'Error';
		header('Location: http://localhost/php_mvc_framework/logged');
		
	}

	public function logout()
	{
		Auth::logout();

	}
}