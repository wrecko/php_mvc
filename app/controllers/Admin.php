<?php

class Admin extends Controller
{

	function __construct()
	{
		$this->authorize(['role' => ENUMS::ADMIN]);
		$this->register_models(['user', 'product']);
		View::setLayout('adminLayout.php');
	}

	public function index()
	{
		return $this->view();
	}

	public function product()
	{
		$products = Product::all();

		return $this->view('product', $products);	
	}

	



}