<?php

class Authorization extends Controller
{

	function __construct()
	{
		$this->register_models(['user']);
		View::setLayout('authLayout.php');
	}

	public function index()
	{
		if(Auth::isAuthorized())
		{ 
			return Redirect::route('admin');			
		}

		$this->view('index');
	}

	public function loginPost(Request $request)
	{
		if(!$request->validate())
		{
			return Redirect::route('login', Localization::getValidationError());
		}
		
		$user = User::fillModel($request->all());			

		if(!empty($user->username) && !empty($user->password) && Auth::authorize($user->username, $user->password))
		{
			return Redirect::route('admin');			
		}

		BTProtection::check($request);
		return Redirect::route('login', Localization::getErrorString('login'));
	}

	public function register()
	{
		$this->view('register');
	}

	public function registerPost(Request $request)
	{
		
	}

	public function logout()
	{
		Auth::logout();
	}


}
