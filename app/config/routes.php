<?php

////////////	Error routes   //////////////
Route::get('404', 'ErrorController@error404');
Route::get('500', 'ErrorController@error500');
Route::get('error', 'ErrorController@generalError');
//////////////////////////////////////////

Route::get('login', 'Authorization@index');
Route::get('logout', 'Authorization@logout');

Route::middleware('auth', ['role' => ENUMS::ADMIN])->group(function(){
	Route::get('admin', 'admin@index');
});



Route::locale('cz', function(){
	Route::get('test/test', 'Home@rep');	
	
});


