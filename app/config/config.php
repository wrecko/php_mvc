<?php

return [
	'endpoint'				=> 'eshop',
	'default_lang' 			=> 'en',
	'dbHost' 				=> 'localhost',
	'dbUser' 				=> 'root',
	'dbPassword' 			=> '',
	'dbTableName' 			=> 'eshop',
	'bruteForceTryLimit'	=> '10',
	'bruteForceTimeLimit'	=> '5',
];


