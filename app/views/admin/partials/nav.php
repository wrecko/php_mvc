<div class='admin-nav'>
	<ul class="menu">
		<li class="menu-item">			
			<a href="/admin/product">
				<span>{products}</span>
			</a>

			<div class="admin-nav-submenu">
				<ul>
					<li><a href="/admin/category">{categories}</a></li>
					<li><a href="">{options}</a></li>

				</ul>
			</div>
		</li>
		<li class="menu-item"><a href="/logout">Logout</a></li>
	</ul>
</div>
