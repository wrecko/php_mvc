<!DOCTYPE html>
<html>
<head>
	<title>Admin panel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">	
	<link rel="stylesheet" href="css/admin/style.css" />
</head>
<body>
	@section('admin/partials/header.php')
	@section('admin/partials/nav.php')

	<div id="admin-container" class="container">
		@RenderBody()
	</div>
	
</body>
</html>