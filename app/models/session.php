<?php

/**
 * 
 */
class Session extends BaseModel
{
	protected $table = 'session';
	protected $hasId = false;

	public $session_id;
	public $user_id;
	public $token;
	public $serial;
	public $created_at;
}