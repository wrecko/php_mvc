<?php

/**
 * 
 */
class User extends BaseModel
{
	protected $table = 'user';

	public $username;
	public $password;
	public $role;
	public $created_at;
	public $updated_at;
}