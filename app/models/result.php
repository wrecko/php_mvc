<?php

/**
 * 
 */
class Result
{
	
	public 	$success,
			$msg;

	function __construct__($success, $msg)
	{
		$this->success = $success;
		$this->msg = $msg;
	}

}