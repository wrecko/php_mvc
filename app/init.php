<?php 

require_once 'core/App.php';
require_once 'core/Controller.php';
require_once 'core/database/db.php';
require_once 'core/database/base_model.php';
require_once 'core/security/auth.php';
require_once 'core/security/bruteforce.php';
require_once 'core/request.php';
require_once 'core/session.php';
require_once 'core/routing/route.php';
require_once 'core/routing/route_model.php';
require_once 'core/routing/redirect.php';
require_once 'core/localization/localization.php';
require_once 'core/configuration/configuration.php';

require_once 'config/enums.php';
require_once 'config/routes.php';

session_start();

